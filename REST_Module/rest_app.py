from flask import Flask
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

items = []

class Item(Resource):
    def get(self, name):
        for item in items:
            if item['name'] == name:
                return item # flask restful does jsonify objects under the hood
        return {'name': None}, 404


    def post(self, name):
        item = {'name': name, 'price': 12.00}
        items.append(item)
        return item, 201


class ItemList(Resource):
    def get(self):
        return items


api.add_resource(Item, "/item/<string:name>")
api.add_resource(ItemList, "/items")


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)