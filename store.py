class Store:
    def __init__(self, name):
        self.name = name
        self.items = []

    def add_items(self, name, price):
        self.items.append({
            'name': name,
            'price': price
        })

    def stock_price(self):
        total = 0
        for item in self.items:
            total += item['price']
        return total

    def __repr__(self):
        return f"store name {self.name}, total stock price is {self.stock_price()}"

    @classmethod
    def franchise(cls, store):
        return cls(store.name + '_' + 'franchise')

    @classmethod
    def store_details(cls):
        pass





store2= Store('Amazon')

store2.add_items('keyboard', 160)

store3 = Store.franchise(store2)

print(store3)
print(store2)