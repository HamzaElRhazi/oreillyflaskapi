from flask import Flask, request, jsonify, render_template
from store import Store

app = Flask(__name__)


# list of dump stores
stores = [
    {
        'name': 'My Wonderful Store',
        'items': [
            {
                'name': 'My item',
                'price': 15.99
            }
        ]
    }
]




@app.route('/')
def home():
    return render_template("index.html")

# POST /store data: {name:}
@app.route("/store", methods=['POST'])
def create_store():
    request_data = request.get_json()
    new_store = {
        'name': request_data['name'],
        'items': []
    }
    stores.append(new_store)
    return jsonify(new_store)

# GET /store/<string : name>
@app.route("/store/<string:name>", methods=['GET'])
def get_store_by_name(name):
    #iterate over stores
    resulting_store ={}
    for store in stores:
        if store['name'] == name:
            resulting_store = store
            return jsonify(resulting_store)
    return jsonify({"error": "store not found"})


# GET /store
@app.route('/store', methods=['GET'])
def get_stores():
    return jsonify({'stores': stores})

# POST /store/<string:name>/item {name:, price:}
@app.route("/store/<string:name>/item", methods=['POST'])
def create_store_item(name):
    for store in stores:
        if store['name'] == name:
            request_data = request.get_json()
            new_item = {
                'name': request_data['name'],
                'price': request_data['price']
            }
            store['items'].append(new_item)
            return jsonify(new_item)
    return jsonify({'message': 'Error, there is no store with the given name'})

# GET /store/<string: name>/item
@app.route("/store/<string:name>/item", methods=['GET'])
def get_items_in_store(name):
    for store in stores:
        if store['name'] == name:
            return jsonify({"items": store['items']})
    return jsonify({'error': "item not found in store"})


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
